const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

const debug = process.env.NODE_ENV !== 'production';

const ROOT_PATH = path.resolve(__dirname);
const ENTRY_PATH = path.resolve(ROOT_PATH, 'src/js/index.js');
const TEMPLATE_PATH = path.resolve(ROOT_PATH, 'src/index.html');
const SHADER_PATH = path.resolve(ROOT_PATH, 'src/shaders');
const BUILD_PATH = path.resolve(ROOT_PATH, 'dist');

module.exports = {
  entry: ENTRY_PATH,
  devtool: 'source-map',
  output: {
    filename: 'bundle.js',
    path: BUILD_PATH
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'WebGL Project Boilerplate',
      template: TEMPLATE_PATH
    }),
    new webpack.DefinePlugin({
      __DEV__: debug
    })
  ],
  module: {
    noParse: /tangram\/dist\/tangram/,
    rules: [
      { test: /\.glsl$/, use: 'webpack-glsl-loader' }
    ]
  }
};
