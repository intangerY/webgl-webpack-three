// import our shaders and store them variables
import boxVertShaderSource from '../shaders/vert.glsl';
import boxFragShaderSource from '../shaders/frag.glsl';
import L from 'leaflet';
import Tangram from 'tangram/dist/tangram.debug';

//const canvas = document.getElementById('canvas');
//const gl = canvas.getContext('experimental-webgl');

//
// canvas.width  = 640;
// canvas.height = 480;
//
// gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
//
// gl.clearColor(1.0, 0.0, 0.0, 1.0);
// gl.clear(gl.COLOR_BUFFER_BIT);

// import {Scene, PerspectiveCamera, WebGLRenderer, BoxGeometry, MeshBasicMaterial, Mesh,
//   ShaderMaterial, Vector2, PlaneGeometry
// } from 'three';
//
// let uniforms = {
// 			time: { type: "f", value: 1.0 },
// 			resolution: { type: "v2", value: new Vector2() }
// 		};
// 	let	materialShader = new ShaderMaterial( {
// 			uniforms: uniforms,
// 			vertexShader: boxVertShaderSource,
// 			fragmentShader: boxFragShaderSource
// 		});
// 	let	mesh = new Mesh( new PlaneGeometry( 2, 2 ), materialShader );
//
//
// const scene = new Scene();
// const camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
//
// const renderer = new WebGLRenderer();
// //scene.add( mesh );
//
// renderer.setSize(window.innerWidth, window.innerHeight);
// document.body.appendChild(renderer.domElement);
//
// const geometry = new BoxGeometry(1, 1, 1);
// const material = new MeshBasicMaterial({color: 0x00ff00});
// const cube = new Mesh(geometry, materialShader);
// scene.add(cube);
//
// camera.position.z = 5;

var map = L.map('map');

var layer = Tangram.leafletLayer({
           scene: '../../scene.yaml',
          //  events: {
          //    hover: function(selection) { console.log('Hover!', selection); },
          //    click: function(selection) { console.log('Click!', selection); }
          // }
       });
       layer.addTo(map);
       map.setView([40.70531887544228, -74.00976419448853], 15);
       //map.setView([37.773972, -122.431297], 15);
       window.layer = layer;
       var scene = layer.scene;
       window.scene = scene;

// function render(){
// 	requestAnimationFrame(render);
// 	cube.rotation.x += 0.1;
// 	cube.rotation.y += 0.1;
// 	renderer.render(scene, camera);
// }

//render();
